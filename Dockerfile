FROM node:alpine
RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community docker
ADD package.json /
ADD node_modules/ /node_modules
RUN npm install
ADD . /
