var express = require('express')
var bodyParser = require('body-parser')
var execSync = require('child_process').execSync;
var app = express()

app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

var handleError = function(error, res) {
  console.log(error.message)
  res.write(JSON.stringify(error.message))
  res.status(400).end();
}

var handleSuccess = function(buffer, res) {
  console.log(buffer.toString('utf8'))
  res.write(buffer.toString('utf8'))
}

app.post('/deploy', function (req, res) {
  res.write(JSON.stringify(req.body)+"\n")

  var dockerpullcmd = "docker pull "+req.body.DOCKER_IMAGE_NAME

  var dockerruncmd = "docker run "+req.body.DOCKER_RUN_OPTIONS
  if (req.body.DOCKER_ENV_FILE) {
    dockerruncmd += " --env-file="+req.body.DOCKER_ENV_FILE
  }
  dockerruncmd += " "+req.body.DOCKER_IMAGE_NAME+" "+req.body.DOCKER_RUN_COMMAND

  var diFilter      = 'docker ps -qa --filter ancestor="'+req.body.DOCKER_IMAGE_NAME+'"'
  var dockerstopcmd = diFilter + " | xargs -r docker stop"
  var dockerrmcmd   = diFilter + " | xargs -r docker rm"

  try {

    handleSuccess(execSync(dockerstopcmd),res)
    handleSuccess(execSync(dockerrmcmd),res)
    handleSuccess(execSync(dockerpullcmd),res)
    handleSuccess(execSync(dockerruncmd),res)

    res.end()
  } catch(err) {
    handleError(err,res)
  }

})

app.listen(5000, function () {
  console.log('Example app listening on port 18088!')
})
